// shuaib abdurahman


let trainer = {
    name: "Bob",
    age: 18,
    Pokemon: ['pika', 'rat'],
    Friend:'dave',
    talk: function(){
        console.log('Pikachu! I choose you!')
    }

}
console.log(trainer)
console.log(trainer.talk)

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target) {
        if(target.health <= 0){
            target.faint();
        }
        else if (target.health != 0){
        target.health--;
		console.log(`${this.name} tackled ${target.name}`);
		console.log("targetPokemon's health is now reduced.")
        console.log(`${target.name}'s is now ${target.health} `);
        }
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}

let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);